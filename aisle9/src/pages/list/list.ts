import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { ItemDetailsPage } from '../item-details/item-details';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    this.items = [{title: 'eggs', note: 'dairy section', icon: 'beer'},
                  {title: 'milk', note: 'dairy section', icon: 'beer'},
                  {title: 'cheese', note: 'dairy section', icon: 'beer'},
                  {title: 'cereal', note: 'aisle 4', icon: 'beer'},
                  {title: 'chips', note: 'aisle 5', icon: 'beer'},
                  {title: 'juice', note: 'aisle 5', icon: 'beer'},
                  {title: 'pancake mix', note: 'aisle 7', icon: 'beer'},
                  {title: 'syrup', note: 'aisle 7', icon: 'beer'},
                  {title: 'oil', note: 'aisle 7', icon: 'beer'},
                  {title: 'wine', note: 'aisle 9', icon: 'beer'},];

    
  }

  itemTapped(event, item) {
    this.navCtrl.push(ItemDetailsPage, {
      item: item
    });
  }
}
